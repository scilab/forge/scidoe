/*
* Copyright (C) 2013 - Michael Baudin
*
* This file must be used under the terms of the CeCILL.
* This source file is licensed as described in the file COPYING, which
* you should have received as part of this distribution.  The terms
* are also available at
* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
*
*/

//
// gw_scidoe.h
//   Header for the SCIDOE gateway.
//
#ifndef __SCI_GW_SCIDOE_H__
#define __SCI_GW_SCIDOE_H__

int sci_scidoe_pdist(char* fname, void* pvApiCtx);

#endif /* __SCI_GW_SCIDOE_H__ */
