// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function distfun_builderSrc()
    src_dir = get_absolute_file_path("builder_src.sce");
	dirarray=["c"]
    tbx_builder_src_lang(dirarray, src_dir);
endfunction
distfun_builderSrc();
clear distfun_builderSrc;
