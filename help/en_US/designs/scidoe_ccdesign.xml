<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from scidoe_ccdesign.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="scidoe_ccdesign" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:scilab="http://www.scilab.org"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>scidoe_ccdesign</refname>
    <refpurpose>A Central Composite Design of Experiments</refpurpose>
  </refnamediv>


<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   H = scidoe_ccdesign(nbvar)
   H = scidoe_ccdesign(nbvar,"Name",value,...)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>nbvar :</term>
      <listitem><para> a 1-by-1 matrix of doubles, positive integer, the number of variables of the experiment</para></listitem></varlistentry>
   <varlistentry><term>Name :</term>
      <listitem><para> a 1-by-1 matrix of strings. The options are "type", "alpha" and "center".</para></listitem></varlistentry>
   <varlistentry><term>value :</term>
      <listitem><para> the value</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
This function produces a Central Composite Design (CCD) of experiments.
A CCD is composed by :
<itemizedlist>
<listitem>
<para>
a factorial block design,
</para>
</listitem>
<listitem>
<para>
a block of axial (star) points and
</para>
</listitem>
<listitem>
<para>
a matrix of center points (zeros).
</para>
</listitem>
</itemizedlist>
   </para>
   <para>
The available options are the following.
<itemizedlist>
<listitem>
<para>
"type" : "circumscribed" (default), "inscribed" or "faced".
</para>
</listitem>
<listitem>
<para>
"alpha" : "orthogonal" (default) or "rotatable".
</para>
</listitem>
<listitem>
<para>
"center" : a 1-by-2 row vector of doubles, the number of center points in each block of the design.
</para>
</listitem>
</itemizedlist>
   </para>
   <para>
   </para>
   <para>
The "circumscribed" and "inscribed" can be rotatable designs,
but "faced" cannot. For "faced" CCD, alpha =1.
If "type" is specified, while "alpha" is not, then default value
is "orthogonal".
   </para>
   <para>
If the design is "orthogonal",
<screen>
alpha = sqrt(nbvar*(1+(nao/na))/(1+(nco/nc))),
</screen>
where
<itemizedlist>
<listitem>
<para>
nc : the number of factorial points,
</para>
</listitem>
<listitem>
<para>
nco : the number of center points added to the factorial design,
</para>
</listitem>
<listitem>
<para>
na : the number of axial points,
</para>
</listitem>
<listitem>
<para>
nao : the number of center points added to the axial points.
</para>
</listitem>
</itemizedlist>
   </para>
   <para>
If the design is "rotatable",
<screen>
alpha = nc^1/4,
</screen>
where nc=2^nbvar are the factorial points.
   </para>
   <para>
If the design is "faced", then alpha=1.
   </para>
   <para>
The user can specify the number of center points in each block of the
design (factorial and axial).
If "Parameter" is [2 3], then there are 2 center points in the
factorial block and 3 points in the axial points block.
Default value is [4 4] (a total of 8 center points in the CCD).
   </para>
   <para>
There is no strict order in the input of the above options.
For example,
<screen>
scidoe_ccdesign(2,"type","inscribed","center",[2 3])
</screen>
will output
the same as
<screen>
scidoe_ccdesign(2,"center",[2 3],"type","inscribed")
</screen>
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Circumscribed orthogonal design
H = scidoe_ccdesign(2)
// Inscribed orthogonal design
H = scidoe_ccdesign(3,"type","inscribed")
// Faced CCD
H = scidoe_ccdesign(2,"type","faced")
// Inscribed rotatable CC Design
H = scidoe_ccdesign(3,"type","inscribed","alpha","rotatable")
// Orthogonal CCD with 2 2 center points in the factorial block
// and 3 center points in the star points block
H = scidoe_ccdesign(3,"center",[2 3])

   ]]></programlisting>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>George E. P. Box, J. Stuart Hunter, William G. Hunter, "Statistics for experimenters Design,Innovation and Discovery", Second Edition, 2005</para>
   <para>http://en.wikipedia.org/wiki/Central_composite_design</para>
   <para>http://rgm2.lab.nig.ac.jp/RGM2/func.php?rd_id=rsm:ccd</para>
   <para>http://www.mathworks.com/help/toolbox/stats/f56635.html</para>
   <para>http://www.itl.nist.gov/div898/handbook/pri/section3/pri3361.htm</para>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2012-2013 - Michael Baudin</member>
   <member>Copyright (C) 2012 - Maria Christopoulou</member>
   <member>Copyright (C) 2009 - Yann Collette</member>
   </simplelist>
</refsection>
</refentry>
