// Copyright (C) 2012 - Maria Christopoulou
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution. The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt␊
// <-- JVM NOT MANDATORY -->
function S=computeSnaive(X)
    m=size(X,"r")
    S=zeros(m,m)
    for i=1:m
        for j=1:m
            S(i,j)=norm(X(i,:)-X(j,:));
        end
    end
endfunction
function checkS(X,S)
    m=size(X,"r")
    for i=1:m
        for j=1:m
            dij=norm(X(i,:)-X(j,:));
            mprintf("%s: Checking [%d,%d]...\n",..
            "checkS",i,j)
            assert_checkalmostequal(S(i,j),dij);
        end
    end
    mprintf("%s: OK\n","checkS")
endfunction
//
// Part 1 : vector > square matrix
//
D = [
    0.5864701  
    0.2523410  
    0.7910114  
    0.3181057  
    0.3447304  
    0.2046658  
    0.9019596  
    0.5484624  
    0.5603024  
    1.1066054  
];
S = scidoe_squareform(D);
Sexpected=[
    0.           0.5864701    0.2523410    0.7910114    0.3181057  
    0.5864701    0.           0.3447304    0.2046658    0.9019596  
    0.2523410    0.3447304    0.           0.5484624    0.5603024  
    0.7910114    0.2046658    0.5484624    0.           1.1066054  
    0.3181057    0.9019596    0.5603024    1.1066054    0.         
];
assert_checkalmostequal(S,Sexpected,[],1.e-5);
//
D = 1.0784668;
S = scidoe_squareform(D,"tomatrix");
Sexpected=[
    0.           1.0784668  
    1.0784668    0.         
];
assert_checkalmostequal(S,Sexpected,[],1.e-5);
//
// Check if empty matrix is generated when we have only one sample point
D = [];
S = scidoe_squareform(D);
assert_checkequal(S,0.);
//
D = [
    0.8164972  
    0.7747868  
    0.3491443  
    1.0542366  
    1.0700159  
    0.8349132  
    0.6052684  
    0.4269890  
    0.8283472  
    0.8572732  
];
S = scidoe_squareform(D);
Sexpected=[
    0.           0.8164972    0.7747868    0.3491443    1.0542366  
    0.8164972    0.           1.0700159    0.8349132    0.6052684  
    0.7747868    1.0700159    0.           0.4269890    0.8283472  
    0.3491443    0.8349132    0.4269890    0.           0.8572732  
    1.0542366    0.6052684    0.8283472    0.8572732    0.         
];
assert_checkalmostequal(S,Sexpected,[],1.e-5);
////////////////////////////////////////////////////////////
//
// Part 2 : square matrix > vector
S=[
    0.           0.5864701    0.2523410    0.7910114    0.3181057  
    0.5864701    0.           0.3447304    0.2046658    0.9019596  
    0.2523410    0.3447304    0.           0.5484624    0.5603024  
    0.7910114    0.2046658    0.5484624    0.           1.1066054  
    0.3181057    0.9019596    0.5603024    1.1066054    0.         
];
D = scidoe_squareform(S);
Dexpected = [
    0.5864701  
    0.2523410  
    0.7910114  
    0.3181057  
    0.3447304  
    0.2046658  
    0.9019596  
    0.5484624  
    0.5603024  
    1.1066054  
]';
assert_checkalmostequal(D,Dexpected,[],1.e-5);
//
S=[
    0.           0.8164972    0.7747868    0.3491443    1.0542366  
    0.8164972    0.           1.0700159    0.8349132    0.6052684  
    0.7747868    1.0700159    0.           0.4269890    0.8283472  
    0.3491443    0.8349132    0.4269890    0.           0.8572732  
    1.0542366    0.6052684    0.8283472    0.8572732    0.         
];
D = scidoe_squareform(S);
Dexpected = [
    0.8164972  
    0.7747868  
    0.3491443  
    1.0542366  
    1.0700159  
    0.8349132  
    0.6052684  
    0.4269890  
    0.8283472  
    0.8572732  
]';
assert_checkalmostequal(D,Dexpected,[],1.e-5);
//
// TODO : check that the diag part of S is zero
// TODO : check that special cases are correctly taken into account
// TODO : check that scalar + "tomatrix" is correctly taken into account
// TODO : check that scalar + "tovector" is correctly taken into account
//
D=scidoe_squareform(0.,"tovector");
assert_checkequal(D,[]);
//
S=scidoe_squareform(13.,"tomatrix");
assert_checkequal(S,[0. 13.;13. 0.]);
//
S=[
    0.           0.8164972    0.7747868    0.3491443    1.0542366  
    0.8164972    0.           1.0700159    0.8349132    0.6052684  
    0.7747868    1.0700159    0.           0.4269890    0.8283472  
    0.3491443    0.8349132    0.4269890    0.           0.8572732  
    1.0542366    0.6052684    0.8283472    0.8572732    0.         
];
D = scidoe_squareform(S,"tovector");
Dexpected = [
    0.8164972  
    0.7747868  
    0.3491443  
    1.0542366  
    1.0700159  
    0.8349132  
    0.6052684  
    0.4269890  
    0.8283472  
    0.8572732  
]';
assert_checkalmostequal(D,Dexpected,[],1.e-5);
//
D = [
    0.8164972  
    0.7747868  
    0.3491443  
    1.0542366  
    1.0700159  
    0.8349132  
    0.6052684  
    0.4269890  
    0.8283472  
    0.8572732  
];
S = scidoe_squareform(D,"tomatrix");
Sexpected=[
    0.           0.8164972    0.7747868    0.3491443    1.0542366  
    0.8164972    0.           1.0700159    0.8349132    0.6052684  
    0.7747868    1.0700159    0.           0.4269890    0.8283472  
    0.3491443    0.8349132    0.4269890    0.           0.8572732  
    1.0542366    0.6052684    0.8283472    0.8572732    0.         
];
assert_checkalmostequal(S,Sexpected,[],1.e-5);
//
// Check backward and forward conversions 
X = [
0.3185689    0.684731   
0.7688531    0.3089766  
0.5430379    0.5694503  
0.9205527    0.1715891  
0.1097627    0.9247127
];
D1=scidoe_pdist(X);
S1=scidoe_squareform(D1);
D2=scidoe_squareform(S1);
assert_checkequal(D1,D2);
S2=scidoe_squareform(D2);
assert_checkequal(S1,S2);
//
// Check backward and forward conversions 
// when there is a single point
X = [
0.3185689    0.684731
];
D1=scidoe_pdist(X);
S1=scidoe_squareform(D1);
D2=scidoe_squareform(S1);
assert_checkequal(D1,D2);
S2=scidoe_squareform(D2);
assert_checkequal(S1,S2);
