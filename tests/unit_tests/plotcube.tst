// Copyright (C) 2012-2013 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Plot a 2D cube:
scf();
scidoe_plotcube(2);

// Plot a 3D cube:
scf();
scidoe_plotcube(3);

// Plot a 2D cube in [0,1]^2
scf();
scidoe_plotcube(2,[0,1]);

// Plot a 3D cube in [0,1]^3
scf();
scidoe_plotcube(3,[0,1]);
