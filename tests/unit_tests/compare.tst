// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008 - INRIA - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

order = scidoe_compare ( 1 , -1 );
assert_checkequal ( order , 1 );
//
order = scidoe_compare ( -1 , 1 );
assert_checkequal ( order , -1 );
//
order = scidoe_compare ( 1 , 1 );
assert_checkequal ( order , 0 );

